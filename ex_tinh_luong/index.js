const LUONG_MOI_GIO = 10;

function tinhLuong() {
    var tongGioLam = document.getElementById("txt-gio-lam").value * 1;
    var tienLuong;
    if (tongGioLam < 40) {
        tienLuong = tongGioLam * LUONG_MOI_GIO;
    } else {

        tienLuong = 40 * LUONG_MOI_GIO + (tongGioLam - 40) * 1.5 * LUONG_MOI_GIO;
    }

    document.getElementById("result").innerHTML = `tien luong cua ban la: ${tienLuong} $`;

}